readme.md - README for GambasGuiGtk3 v0.16


Purpose:
Template GUI (Gtk3) with menu, toolbar, statusbar, and docking/anchoring. Majority of menu/button functions are not coded. 

Requires DockAnchor library 0.9 or later.  Requires GambasAppUtilityLib library 0.4 or later. Requires GambasSomeModelLib 0.4 or later. Requires GambasGuiAppUtilityLib library 0.4 or later. 

Note: Gambas3 projects ('.project') are looking for libraries ('<filename>.gambas') in /home/<user_name>/.local/share/gambas3/lib/<vendor_prefix>/.Make (Project|Make|Executable...) writes the executable there in addition to the root of the project's directory.

![GambasGuiGtk3.png](./GambasGuiGtk3.png?raw=true "Screenshot")


Usage notes:

~...


Setup:

~...


0.16: 
~add license file
~add readme
0.15: 
~fix checks for dirty/save/cancel before new/open, and add check to exit
0.14: 
~perform internationalization check of all strings in code
0.13: 
~remove unused control references;v0.12: fix settings key name save; add v0.12: fix settings key name save; add updating of form title;
0.11: 
~swap docking / anchoring on GridView vs TextArea, so former expands; code grid with snapshot of Settings.
0.10: 
~refactor status bar logic into separate lib.
0.9: 
~refactor SomeModel into separate lib share with all GUI apps; add missing Change event on SomeDateTime field.
0.8:
~expand SomeModel and reorganize form
0.7: 
~switch to renamed GambasAppUtilityLib
0.6: 
~switch to AppUtility for Log, Dialogs
0.5: 
~fix Log.FormatError and calls
0.4: 
~refactored to use Action objects to dispatch menu/button clicks.
0.3: 
~updated to use DockAnchor library 0.3 and showcase docking or anchoring inside container controls
0.2: 
~updated to use DockAnchor library 0.2 and showcase  docking stacked or sharing corners.
0.1: 
~initial release with DockAnchor library 0.1 to showcase simple docking (on opposite sides) and anchoring.

Fixes:

Known Issues:
~

Possible Enhancements:


Steve Sepan
ssepanus@yahoo.com
2/14/2022